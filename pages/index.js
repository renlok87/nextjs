import Head from 'next/head'

import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/functions';
import 'firebase/database';
import React from 'react';
import { Provider } from 'react-redux';
import { combineReducers, createStore } from 'redux';
import { ReactReduxFirebaseProvider,  firebaseReducer,
  firestoreReducer } from 'react-redux-firebase';
import { firebaseConfig, reduxFirebase } from '../firebaseConfig';
import { createFirestoreInstance } from 'redux-firestore';
import HomeContainer from '../components/HomeContainer/HomeContainer';
const initialState = {};
const rootReducer = combineReducers({
  firebase: firebaseReducer,
  firestore: firestoreReducer // <- needed if using firestore
})
const store = createStore(rootReducer, initialState)

!firebase.apps.length
    ? firebase.initializeApp(firebaseConfig).firestore()
    : firebase.app().firestore();

firebase.functions();

const Home = () => (
  <div className="container">
    <Head>
      <title>Create Next App</title>
      <link rel="icon" href="/favicon.ico" />
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="theme-color" content="#000000" />
    <meta
      name="description"
      content="Web site created using create-react-app"
    />

    <title>Web Hero School</title>
    </Head>

    <main>
    <Provider store={store}>
        <ReactReduxFirebaseProvider
            firebase={firebase}
            config={reduxFirebase}
            dispatch={store.dispatch}
            createFirestoreInstance={createFirestoreInstance}
        >
          
            <HomeContainer />
        </ReactReduxFirebaseProvider>
    </Provider>
    </main>
  </div>
)

export default Home
