import { useEffect, useState } from 'react';
import firebase from 'firebase';

export const useGetImage = (url: string) => {
    const [imgUrls, setImgUrls] = useState<string[]>([]);

    useEffect(() => {
        const storage = firebase.storage();
        const gsReference = storage.refFromURL('gs://wh-hero.appspot.com/');
        const listRef = gsReference.child(url);

        listRef
            .listAll()
            .then(result => {
                result.items.forEach(itemRef => {
                    itemRef
                        .getDownloadURL()
                        .then(url => setImgUrls(imgUrls => [...imgUrls, url]));
                });
            })
            // eslint-disable-next-line no-console
            .catch(error => console.log('error', error));
    }, [url]);

    return imgUrls;
};
