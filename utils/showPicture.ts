import { Func } from '@tinkoff/utils/typings/types';

export const showPicture: Func = (imgTeacher: string, pictureList: string[]) => {
    if (!imgTeacher) return '';

    for (let i = 0; i < pictureList.length; i++) {
        if (pictureList[i].includes(imgTeacher)) {
            return pictureList[i];
        }
    }
};
