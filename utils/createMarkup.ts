export const createMarkup = <T>(text: T) => {
    return { __html: text };
};
