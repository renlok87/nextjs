import React from 'react';
//types
import { Func } from '@tinkoff/utils/typings/types';
//styles
import styles from './TableRow.module.css';
//components
import Text from '../Text/Text';
import Photo from '../FotoTeacher/Teacher';

interface ITableRowProps {
    fullname: string;
    position: string;
    urlPhotoTeacher: string;
    urlBackgroundTeacher: string;
    onClick: Func;
}

const TableRow: React.FC<ITableRowProps> = ({
    fullname,
    position,
    urlPhotoTeacher,
    urlBackgroundTeacher,
    onClick,
}) => (
    <tr className={styles.tableRow} onClick={onClick}>
        <td className={styles.tdLeftSide}>
            <Text variant="h5">{fullname}</Text>
        </td>
        <td className={styles.tdRightSide}>
            <Text variant="p2">{position}</Text>
        </td>
        <div className={styles.trPhoto}>
            <Photo
                urlBackgroundTeacher={urlBackgroundTeacher}
                urlFotoTeacher={urlPhotoTeacher}
            ></Photo>
        </div>
    </tr>
);

export default TableRow;
