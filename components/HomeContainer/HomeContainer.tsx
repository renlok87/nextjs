import React, { useEffect, useState } from 'react';
import { isLoaded, isEmpty, useFirebaseConnect } from 'react-redux-firebase';
import { useSelector } from 'react-redux';
import HomePage from '../HomePage/HomePage';
import Loader from '../Loader/Loader';
const Data ={
    data: {
      bannerLesson: {
        bannerLessonSubtitle: 'Участвуй в бесплатном 3-дневном марафоне и узнай, с&nbsp;чего&nbsp;начать&nbsp;изучать&nbsp; веб-разработку',
        bannerLessonTitle: 'Сомневаешься, \n что программирование — это твое?',
        btnText: 'Открыть урок',
        textLink: 'https://lk.webheroschool.ru/reg_webmarafon_site'
      },
      blockBounties: {
        cardsBounty: [
          {
            imgBackHover: '',
            imgBackUrl: 'star_purple.svg',
            imgUrl: 'game.svg',
            imgUrlHover: 'giphy-11.gif',
            textSubtitle: 'Выполняй задания и получай&nbsp;баллы. Объединяйся в&nbsp;команды&nbsp;с другими&nbsp;студентами&nbsp;и соревнуйся за&nbsp;призы',
            textTitle: 'Геймификация'
          },
          {
            imgBackHover: '',
            imgBackUrl: 'star_blue.svg',
            imgUrl: 'lesson.svg',
            imgUrlHover: '200-4.gif',
            textSubtitle: 'Знакомься с разработчиками из IT-компаний, получай доступ к многолетнему опыту и задавай личные вопрос',
            textTitle: 'Лекции с гостями'
          },
          {
            imgBackHover: '',
            imgBackUrl: 'star_purple.svg',
            imgUrl: 'fullstack.svg',
            imgUrlHover: '200-7.gif',
            textSubtitle: 'Отрабатывай навыки на&nbsp;дополнительных офлайн&nbsp;-&nbsp;воркшопах',
            textTitle: 'Воркшопы'
          },
          {
            imgBackHover: '',
            imgBackUrl: 'star_yellow.svg',
            imgUrl: 'peer_to_peer.svg',
            imgUrlHover: 'giphy-12.gif',
            textSubtitle: 'Получай поддержку от&nbsp;опытных студентов и&nbsp;помогай тем, кто идет по&nbsp;твоим следам',
            textTitle: 'Peer-to-peer обучени'
          },
          {
            imgBackHover: '',
            imgBackUrl: 'star_red.svg',
            imgUrl: 'company.svg',
            imgUrlHover: '200-8.gif',
            textSubtitle: 'Онлайн и&nbsp;офлайн экскурсии в&nbsp;крупные IT-компании. В&nbsp;каком офисе хотел&nbsp;бы&nbsp;побывать?',
            textTitle: 'Экскурсии в IT-компании'
          },
          {
            imgBackHover: '',
            imgBackUrl: 'star_blue_big.svg',
            imgUrl: 'community.svg',
            imgUrlHover: '200-6.gif',
            textSubtitle: '20&nbsp;000+ участников сообщества&nbsp; и&nbsp;400+ студентов школы. Достигай результата с&nbsp;единомышленниками вдвое быстрей!',
            textTitle: 'Сообщество'
          },
          {
            imgBackHover: '',
            imgBackUrl: 'star_red_big.svg',
            imgUrl: 'meeting.svg',
            imgUrlHover: '200-5.gif',
            textSubtitle: 'Продолжай дружить с&nbsp;одногруппниками на&nbsp;офлайн&#8209;встречах школы',
            textTitle: 'Живые встречи'
          }
        ],
        link: '#',
        photosBounty: [
          'IMG_5561.webp',
          'IMG_201.jpg',
          'IMAGE_2019.jpg'
        ]
      },
      blockComposition: {
        link: '#'
      },
      blockTeam: {
        link: '#',
        teachers: [
          {
            id: 0,
            imgTeacher: 'foto_Darya.webp',
            logos_img: [
              'tinkoff.svg',
              'mts.svg',
              'purrweb.svg',
              'urlLogoFirmsFourth'
            ],
            popupTeacherAbout: 'Frontend-разработчик (2GIS, Mail.ru, Tinkoff.ru) и автор школы по веб - разработке с нуля и до трудоустройства, Web Hero School',
            subtitle: 'Основатель и преподаватель',
            title: 'Дарья Пушкарская',
            urlBackgroundTeacher: 'back_Darya.svg'
          },
          {
            id: 1,
            imgTeacher: 'julia-bazarova.webp',
            logos_img: [
              ''
            ],
            popupTeacherAbout: '',
            subtitle: 'HR',
            title: 'Юлия Базарова',
            urlBackgroundTeacher: 'backTeacher.svg'
          },
          {
            id: 2,
            imgTeacher: 'anastasia-lukinih.webp',
            logos_img: [
              ''
            ],
            popupTeacherAbout: '',
            subtitle: 'Преподаватель',
            title: 'Анастасия Лукиных',
            urlBackgroundTeacher: 'backTeacher.svg'
          },
          {
            id: 3,
            imgTeacher: 'ekateruna-aplitova.webp',
            logos_img: [
              ''
            ],
            popupTeacherAbout: '',
            subtitle: 'Ментор',
            title: 'Екатерина Анплитова',
            urlBackgroundTeacher: 'backTeacher.svg'
          },
          {
            id: 4,
            imgTeacher: 'artem-panin.webp',
            logos_img: [
              ''
            ],
            popupTeacherAbout: '',
            subtitle: 'Прогресс-менеджер',
            title: 'Артем Панин',
            urlBackgroundTeacher: 'backTeacher.svg'
          },
          {
            id: 5,
            imgTeacher: 'ksenia-burnasheva.webp',
            logos_img: [
              ''
            ],
            popupTeacherAbout: '',
            subtitle: 'Ментор',
            title: 'Ксения Бурнашова',
            urlBackgroundTeacher: 'backTeacher.svg'
          },
          {
            id: 6,
            imgTeacher: 'anna-havina.webp',
            logos_img: [
              ''
            ],
            popupTeacherAbout: '',
            subtitle: 'Ментор',
            title: 'Анна Хавина',
            urlBackgroundTeacher: 'backTeacher.svg'
          },
          {
            id: 7,
            imgTeacher: 'elena-zamina.webp',
            logos_img: [
              ''
            ],
            popupTeacherAbout: '',
            subtitle: 'Менеджер отдела заботы',
            title: 'Елена Зимина',
            urlBackgroundTeacher: 'backTeacher.svg'
          },
          {
            id: 8,
            imgTeacher: 'sergey-pavlihin.webp',
            logos_img: [
              ''
            ],
            popupTeacherAbout: '',
            subtitle: 'Преподаватель',
            title: 'Сергей Павлихин',
            urlBackgroundTeacher: 'backTeacher.svg'
          },
          {
            id: 9,
            imgTeacher: 'alexey-denisov.webp',
            logos_img: [
              ''
            ],
            popupTeacherAbout: '',
            subtitle: 'Ментор',
            title: 'Алексей Денисов',
            urlBackgroundTeacher: 'backTeacher.svg'
          },
          {
            id: 10,
            imgTeacher: 'ksenia-pyagay.webp',
            logos_img: [
              ''
            ],
            popupTeacherAbout: '',
            subtitle: 'Ментор',
            title: 'Ксения Пягай',
            urlBackgroundTeacher: 'backTeacher.svg'
          },
          {
            id: 11,
            imgTeacher: 'anastasia-caregorodceva.webp',
            logos_img: [
              ''
            ],
            popupTeacherAbout: '',
            subtitle: 'Руковолитель отдела заботы',
            title: 'Анастасия Царегородцева',
            urlBackgroundTeacher: 'backTeacher.svg'
          },
          {
            id: 12,
            imgTeacher: 'aygerim.webp',
            logos_img: [
              ''
            ],
            popupTeacherAbout: '',
            subtitle: 'Преподаватель',
            title: 'Айгерим Жанабаева',
            urlBackgroundTeacher: 'backTeacher.svg'
          },
          {
            id: 13,
            imgTeacher: 'sergey-gerasimov.webp',
            logos_img: [
              ''
            ],
            popupTeacherAbout: '',
            subtitle: 'Ментор',
            title: 'Сергей Герасимов',
            urlBackgroundTeacher: 'backTeacher.svg'
          },
          {
            id: 14,
            imgTeacher: 'roman-kirchin.webp',
            logos_img: [
              ''
            ],
            popupTeacherAbout: '',
            subtitle: 'Прогресс-менеджер',
            title: 'Роман Киршин',
            urlBackgroundTeacher: 'backTeacher.svg'
          },
          {
            id: 15,
            imgTeacher: 'anna-obuhova.webp',
            logos_img: [
              ''
            ],
            popupTeacherAbout: '',
            subtitle: 'Менеджер отдела заботы',
            title: 'Анна Обухова',
            urlBackgroundTeacher: 'backTeacher.svg'
          }
        ]
      },
      blockTiltCards: {
        link1: '#',
        link2: '#',
        link3: '#',
        link4: '#'
      },
      cards_Garant: [
        {
          imgBackHover: '',
          imgBackUrl: 'star_purple.svg',
          imgUrl: 'refund.svg',
          imgUrlHover: '',
          textSubtitle: 'Если не понравится формат или \n качество уроков в течение первых 2 \n недель, то вернем полную стоимость \n без лишних вопросов',
          textTitle: 'Возврат средств'
        },
        {
          imgBackHover: '',
          imgBackUrl: 'star_blue.svg',
          imgUrl: '4_attempt.svg',
          imgUrlHover: '',
          textSubtitle: 'Если почувствуйте, что материал \n не усвоился, то пройдите уроки \n сначала 4 раза',
          textTitle: '4 попытки'
        },
        {
          imgBackHover: '',
          imgBackUrl: 'star_purple.svg',
          imgUrl: 'employment.svg',
          imgUrlHover: '',
          textSubtitle: 'Если за год пройдете все модули \n профессии 4 раза, сдадите \n аттестацию и не устроитесь \n на работу, то вернём полную \n стоимость + 10% сверху',
          textTitle: 'Трудоустройство'
        }
      ],
      cards_Profession: [
        {
          imgBackHover: '',
          imgBackUrl: '',
          imgUrl: 'frontend.svg',
          imgUrlHover: '',
          textSubtitle: 'Средняя зарплата: 138 000 ₽/месяц',
          textTitle: 'Frontend'
        },
        {
          imgBackHover: '',
          imgBackUrl: '',
          imgUrl: 'backend.svg',
          imgUrlHover: '',
          textSubtitle: 'Средняя зарплата: 144 000 ₽/месяц',
          textTitle: 'Backend'
        },
        {
          imgBackHover: '',
          imgBackUrl: '',
          imgUrl: 'serp.svg',
          imgUrlHover: '',
          textSubtitle: 'Средняя зарплата: 150 000 ₽/месяц',
          textTitle: 'Fullstack'
        }
      ],
      modules: [
        {
          arrayProfessions: [
            'Frontend',
            'Backend'
          ],
          id: 1,
          link: '#',
          linkText: ' Демо урок',
          subtitle: 'Краткое описание',
          title: ' Подготовка к разработке проекта'
        },
        {
          arrayProfessions: [
            'Frontend',
            'Backend'
          ],
          id: 2,
          link: '#',
          linkText: 'Демо урок',
          subtitle: 'Краткое описание',
          title: 'Создание сайта с использованием HTML и CSS'
        },
        {
          arrayProfessions: [
            'Frontend',
            'Backend'
          ],
          id: 3,
          link: '#',
          linkText: 'Демо урок',
          subtitle: 'Краткое описание',
          title: 'Создание игры на JavaScript'
        },
        {
          arrayProfessions: [
            'Frontend',
            'Backend'
          ],
          id: 4,
          link: '#',
          linkText: 'Демо урок',
          subtitle: 'Краткое описание',
          title: 'Создание приложения на React'
        },
        {
          arrayProfessions: [
            'Frontend',
            'Backend'
          ],
          id: 5,
          link: '#',
          linkText: 'Демо урок',
          subtitle: 'Краткое описание',
          title: 'Поиск работы мечты'
        },
        {
          arrayProfessions: [
            'Frontend',
            'Backend'
          ],
          id: 6,
          link: '#',
          linkText: 'Демо урок',
          subtitle: 'Краткое описание',
          title: 'Работа на фриланс'
        }
      ]
    }
  };
  // const mainData = function getStaticProps(context) {
  //   return {
  //     props: Data, // will be passed to the page component as props
  //   };
  // }
// @ts-ignore
const HomeContainer: React.FC = () => {
  useFirebaseConnect([{ path: '/DB' }]);
    const data = useSelector((state) => {
        return state.firebase.data.DB;
    });
    
 async function getStaticProps() {
    // Fetch data from external API
    
    // Pass data to the page via props
    return { props: { Data } }
  }
   

    if (!isLoaded(data)) {
        return <Loader />;
    }

    if (isEmpty(data)) {
        return <div>No Notes Found</div>;
    }

    if (Data) {
        // @ts-ignore
        return <HomePage data={Data.data} />;
    }
};

export default HomeContainer;