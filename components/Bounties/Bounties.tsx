import React, { useState } from 'react';
import styles from './Bounties.module.css';
import Text from '../Text/Text';
import { Func } from '@tinkoff/utils/typings/types';
import Card from '../Card/Card';
import Link from '../Link/Link';
import TextWithBackground from '../Text/textWithBackground/TextWithBackground';
import PopupSlider from '../Popup/Slider/PopupSlider';
import className from 'classnames/bind';
import { useGetImage } from '../../utils/hooks/useGetImage';
import { showPicture } from '../../utils/showPicture';

type BountiesCardProps = {
    textTitle: string;
    textSubtitle: string;
    imgUrl: string;
    imgBackUrl: string;
    imgUrlHover: string;
    imgBackHover: string;
};

type CardsBounty = {
    cardsBounty: BountiesCardProps[];
    photosBounty: string[];
    link: string;
};

interface IBounties {
    getInfo?: Func;
    blockData: CardsBounty;
}
const cx = className.bind(styles);

const Bounties: React.FC<IBounties> = ({ getInfo, blockData }) => {
    const [isOpenModal, setIsOpenModal] = useState(false);
    React.useEffect(() => {
        console.log("window.innerHeight", window.innerWidth);
      }, []);
    const handleClick = () => {
        setIsOpenModal(!isOpenModal);
    };

    const imgCardsUrlList = useGetImage('/img/cards');
    const imgGifUrlList = useGetImage('/img/gif');
    const sliderPhotoList = useGetImage('/img/photoForBountiesSlider');

    return (
        <div className={styles.wrap}>
            <div className={styles.title}>
                <Text variant={'h2'}>Плюшки \( ﾟヮﾟ)/</Text>
            </div>
            <div className={styles.subtitle}>
                <TextWithBackground
                    isBold={false}
                    variant="p2"
                    textsArray={[
                        { text: 'Твое ' },
                        { text: 'сообщество ', withBackground: true },
                        {
                            text: ' единомышленников с оффлайн-встречами и ',
                        },
                        { text: ' экскурсиями в IT-компании', withBackground: true },
                    ]}
                />
            </div>
            <div className={cx('bountyItems')}>
                {blockData.cardsBounty.map((item, index) => (
                    <div
                        className={cx('sliderMargin', index > 3 && 'cardHidden')}
                        key={index}
                    >
                        <Card
                            textTitle={item.textTitle}
                            textSubtitle={item.textSubtitle}
                            cardStyle="cardBounty"
                            widthCard={160}
                            heigthCard={295}
                            imgUrl={showPicture(item.imgUrl, imgCardsUrlList)}
                            imgBackUrl={showPicture(item.imgBackUrl, imgCardsUrlList)}
                            imgUrlHover={showPicture(item.imgUrlHover, imgGifUrlList)}
                            titleFont="h5"
                        />
                    </div>
                ))}
            </div>
            <PopupSlider
                isOpened={isOpenModal}
                onCloseHandler={() => setIsOpenModal(false)}
                sliderPictures={sliderPhotoList}
            />
            <div onClick={handleClick}>
                <div
                    className={styles.photos}
                    onClick={() => {
                        setIsOpenModal(true);
                    }}
                >
                    <img
                        src={showPicture(blockData.photosBounty[0], sliderPhotoList)}
                        alt="photo_mail"
                        className={styles.photo_mail}
                    />
                    <img
                        src={showPicture(blockData.photosBounty[1], sliderPhotoList)}
                        alt="photo_school_bday"
                        className={styles.photo_school_bday}
                    />
                    <img
                        src={showPicture(blockData.photosBounty[2], sliderPhotoList)}
                        alt="photo_conference"
                        className={styles.photo_conference}
                    />
                </div>
            </div>
            <div className={cx('bountyItems', 'blockHidden')}>
                {blockData.cardsBounty.slice(4).map((item, index) => (
                    <div className={cx('sliderMargin')} key={index}>
                        <Card
                            textTitle={item.textTitle}
                            textSubtitle={item.textSubtitle}
                            cardStyle="cardBounty"
                            widthCard={160}
                            heigthCard={295}
                            imgUrl={showPicture(item.imgUrl, imgCardsUrlList)}
                            imgBackUrl={showPicture(item.imgBackUrl, imgCardsUrlList)}
                            imgUrlHover={showPicture(item.imgUrlHover, imgGifUrlList)}
                            titleFont="h5"
                        />
                    </div>
                ))}
            </div>
            <div className={styles.link}>
                <Link
                    textVariant="link"
                    children="Хочу так!"
                    isBold={true}
                    isArrowSmall={true }
                    link={blockData.link}
                />
            </div>
        </div>
    );
};

export default Bounties;
