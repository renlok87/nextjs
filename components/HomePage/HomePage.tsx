import React from 'react';
import dynamic from 'next/dynamic'
import styles from '../HomePage/HomePage.module.scss';
import Header from '../Header/Header';
import BlockStudent from '../BlockStudent/BlockStudent';
const Footer = dynamic(() => import('../Footer/Footer'));
const BlockForYou = dynamic(() => import('../BlockForYou/BlockForYou'));
const Modules = dynamic(() => import('../Modules/Modules'));
const TiltCards = dynamic(() => import('../TiltCards/TiltCards'));
const Bounties = dynamic(() => import('../Bounties/Bounties'));
const BannerLesson = dynamic(() => import('../BannerLesson/BannerLesson'));
const CardGaranti = dynamic(() => import('../CardGaranti/CardGaranti'));
const CardProfessions = dynamic(() => import('../CardProfessions/CardProfessions'));
const CardFormat = dynamic(() => import('../CardFormat/CardFormat'));
const CardComposition = dynamic(() => import('../CardComposition/CardComposition'));
const BlockTeam = dynamic(() => import('../BlockTeam/BlockTeam'));
const ReviewSlider = dynamic(() => import('../ReviewSlider/ReviewSlider'));

const HomePage: React.FC = data => {
    return (
        <div className={styles.MainContainer}>
            <Header />
            <div className={styles.SectionContainer}>
                <BlockStudent />
            </div>
    
                <div className={styles.SectionContainer}>
                    {/*@ts-ignore*/}
                    <CardProfessions blockData={data.data.cards_Profession} />
                    {/*@ts-ignore*/}
                    <Modules blockData={data.data.modules} />
                </div>
                {/*@ts-ignore*/}
                <BannerLesson blockData={data.data.bannerLesson} />
                <div className={styles.SectionContainer}>
                    <ReviewSlider />
                    <BlockForYou />
                    {/*@ts-ignore*/}
                    <CardGaranti blockData={data.data.cards_Garant} />
                    {/*@ts-ignore*/}
                    <TiltCards blockData={data.data.blockTiltCards} />
                    <CardFormat />
                    {/*@ts-ignore*/}
                    <CardComposition blockData={data.data.blockComposition} />
                    {/*@ts-ignore*/}
                    <Bounties blockData={data.data.blockBounties} />
                    {/*@ts-ignore*/}
                    <BlockTeam blockData={data.data.blockTeam} />
                    <Footer />
                </div>
        </div>
    );
};

export default HomePage;