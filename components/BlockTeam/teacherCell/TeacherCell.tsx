import React from 'react';
import styles from './TeacherCell.module.css';
import Text from '../../Text/Text';
import TextWithBackground from '../../Text/textWithBackground/TextWithBackground';
import { Func } from '@tinkoff/utils/typings/types';

interface ITeacherCellProps {
    title: string;
    subtitle: string;
    imgTeacher: string;
    onClick?: Func;
}

const TeacherCell: React.FC<ITeacherCellProps> = ({
    title,
    subtitle,
    imgTeacher,
    onClick,
}) => {
    return (
        <div className={styles.columnOne__first}>
            <div
                className={styles.columnOne__Img}
                style={{
                    backgroundImage: `url('${imgTeacher}'),
                                        url('/img/teacherFotos/blockTeam/purple-star.svg')`,
                }}
                onClick={onClick}
            ></div>
            <div className={styles.columnOne__firstImgBack}></div>
            <div className={styles.columnOne__firstTitle} onClick={onClick}>
                <Text variant="h4" isBold={true}>
                    {title}
                </Text>
            </div>
            <TextWithBackground
                isBold={true}
                variant="p3"
                textsArray={[{ text: subtitle, withSmallBackground: true }]}
            />
        </div>
    );
};

export default TeacherCell;
