import React, { useState } from 'react';
import Text from '../Text/Text';
import styles from './BlockTeam.module.css';
import Link from '../Link/Link';
import TeacherCell from './teacherCell/TeacherCell';
import { Func } from '@tinkoff/utils/typings/types';
import TextWithBackground from '../Text/textWithBackground/TextWithBackground';
import PopupTeacher from '../Popup/Teacher/PopupTeacher';
import { useGetImage } from '../../utils/hooks/useGetImage';
import { showPicture } from '../../utils/showPicture';

type TeachersList = {
    id: number;
    imgTeacher: string;
    logos_img: string[];
    popupTeacherAbout: string;
    subtitle: string;
    title: string;
    urlBackgroundTeacher: string;
};

type BlockTeamProps = {
    teachers: TeachersList[];
    link: string;
};

interface IBlockTeam {
    blockData: BlockTeamProps;
}

const TITLE = 'Наша команда';
const SUBTITLE1 =
    'Над твоим трудоустройством будут работать — преподаватель, личный прогресс - менеджер, ментор, HR ';
const SUBTITLE2 = 'Готовы помочь 24 / 7';
const TEXT_LINK = 'Задать вопрос в отдел заботы';

const BlockTeam: React.FC<IBlockTeam> = ({ blockData }) => {
    const teacherPhotosList = useGetImage('/img/teacherFotos/blockTeam');
    const teacherBackgroundList = useGetImage('/img/teacherFotos');
    const teacherFirmsList = useGetImage('/img/teacherFirms');
    React.useEffect(() => {
        console.log("window.innerHeight", window.innerWidth);
      }, []);
    const showPictureList: Func = (pictureList, firmsList, index) => {
        if (pictureList[0] === '') return '';

        // eslint-disable-next-line array-callback-return
        return firmsList.map((item: string) => {
            for (let i = 0; i < pictureList.length; i++) {
                if (item.includes(pictureList[i])) {
                    return item;
                }
                continue;
            }
        });
    };

    const [showModal, setValue] = useState(false);
    const [TeacherId, setTeacherId] = useState(null);
    const splitArr: Func = (items, chunks) =>
        [...Array(chunks)].map((_, c) =>
            items.filter((n: any, i: number) => i % chunks === c),
        );

    const newList = splitArr(blockData.teachers, 4);

    const popupShow = (index: number) => {
        if (showModal)
            return (
                <PopupTeacher
                    isOpened={true}
                    onCloseHandler={() => {
                        setValue(false);
                        setTeacherId(null);
                    }}
                    popupTeacherTitle={blockData.teachers[index].title}
                    popupTeacherSubtitle={blockData.teachers[index].subtitle}
                    popupTeacherAbout={blockData.teachers[index].popupTeacherAbout}
                    logos_img={showPictureList(
                        blockData.teachers[index].logos_img,
                        teacherFirmsList ? teacherFirmsList : [],
                        index,
                    )}
                    urlFotoTeacher={showPicture(
                        blockData.teachers[index].imgTeacher,
                        teacherPhotosList ? teacherPhotosList : [],
                    )}
                    urlBackgroundTeacher={showPicture(
                        blockData.teachers[index].urlBackgroundTeacher,
                        teacherBackgroundList ? teacherBackgroundList : [],
                    )}
                >
                    {blockData.teachers[index].title}
                </PopupTeacher>
            );
    };

    if (TeacherId) {
        return (
            <div className={styles.blockTeam}>
                <div className={styles.titleWrap}>
                    <Text variant="h2" isBold={true}>
                        {TITLE}
                    </Text>
                </div>
                <div>{popupShow(Number(TeacherId))}</div>
                <div className={styles.subtitleWrap}>
                    <TextWithBackground
                        isMedium={true}
                        variant="p2"
                        textsArray={[
                            { text: SUBTITLE1 },
                            { text: SUBTITLE2, withBackground: true },
                        ]}
                    />
                </div>

                <div className={styles.contentWrap}>
                    {newList.map((item: [string], index: number) =>
                        index % 2 === 0 ? (
                            <div className={styles.CardWrap}>
                                {newList[index].map((item: any, index: number) => (
                                    <TeacherCell
                                        onClick={() => {
                                            setValue(true);
                                            setTeacherId(item.id);
                                        }}
                                        key={index}
                                        title={item.title}
                                        subtitle={item.subtitle}
                                        imgTeacher={showPicture(
                                            item.imgTeacher,
                                            teacherPhotosList,
                                        )}
                                    />
                                ))}
                            </div>
                        ) : (
                            <div className={styles.columnTwo}>
                                {newList[index].map((item: any, index: number) => (
                                    <TeacherCell
                                        onClick={() => {
                                            setValue(true);
                                            setTeacherId(item.id);
                                        }}
                                        key={index}
                                        title={item.title}
                                        subtitle={item.subtitle}
                                        imgTeacher={showPicture(
                                            item.imgTeacher,
                                            teacherPhotosList,
                                        )}
                                    />
                                ))}
                            </div>
                        ),
                    )}
                </div>
                <div className={styles.link}>
                    <Link
                        textVariant="link"
                        isArrowBig={true}
                        isArrowSmall={false}
                        isBold
                        link={'#'}
                    >
                        {TEXT_LINK}
                    </Link>
                </div>
            </div>
        );
    }

    return (
        <div className={styles.blockTeam}>
            <div className={styles.titleWrap}>
                <Text variant="h2" isBold={true}>
                    {TITLE}
                </Text>
            </div>
            <div>{popupShow(Number(TeacherId))}</div>
            <div className={styles.subtitleWrap}>
                <TextWithBackground
                    isMedium={true}
                    variant="p2"
                    textsArray={[
                        { text: SUBTITLE1 },
                        { text: SUBTITLE2, withBackground: true },
                    ]}
                />
            </div>

            <div className={styles.contentWrap}>
                {newList.map((item: [string], index: number) =>
                    index % 2 === 0 ? (
                        <div className={styles.CardWrap}>
                            {newList[index].map((item: any, index: number) => (
                                <TeacherCell
                                    onClick={() => {
                                        setTeacherId(item.id);
                                        setValue(true);
                                    }}
                                    title={item.title}
                                    subtitle={item.subtitle}
                                    imgTeacher={showPicture(
                                        item.imgTeacher,
                                        teacherPhotosList,
                                    )}
                                />
                            ))}
                        </div>
                    ) : (
                        <div className={styles.columnTwo} key={index}>
                            {newList[index].map((item: any, index: number) => (
                                <TeacherCell
                                    onClick={() => {
                                        setTeacherId(item.id);
                                        setValue(true);
                                    }}
                                    key={index}
                                    title={item.title}
                                    subtitle={item.subtitle}
                                    imgTeacher={showPicture(
                                        item.imgTeacher,
                                        teacherPhotosList,
                                    )}
                                />
                            ))}
                        </div>
                    ),
                )}
            </div>
            <div className={styles.link}>
                <Link
                    textVariant="link"
                    isArrowBig={true}
                    isArrowSmall={false}
                    isBold
                    link={blockData.link}
                >
                    {TEXT_LINK}
                </Link>
            </div>
        </div>
    );
};

export default BlockTeam;
