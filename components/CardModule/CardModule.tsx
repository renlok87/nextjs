import React, { Fragment } from 'react';
import styles from './CardModule.module.css';
import classnames from 'classnames/bind';
import Text from '../Text/Text';
import Badge from '../Badge/Badge';
import Link from '../Link/Link';
import { Func } from '@tinkoff/utils/typings/types';

const cx = classnames.bind(styles);

interface ICardModuleProps {
    title: string;
    subtitle: string;
    linkText: string;
    link: string;
    arrayProfessions: string[];
    onClick?: Func;
    id: number;
}

const CardModule: React.FC<ICardModuleProps> = ({
    title,
    subtitle,
    linkText,
    link,
    arrayProfessions,
    children,
    onClick,
    id,
}) => {
    return (
        <div
            className={cx('card', {
                cardContentText: title,
                cardContainer: !title,
            })}
            onClick={onClick}
        >
            <div>
                {title && (
                    <div className={cx('cardTitle', 'cardText')}>
                        <Text variant="h4" isBold>
                            {title}
                        </Text>
                    </div>
                )}
                {subtitle && (
                    <div className={cx('cardSubtitle', 'cardText')}>
                        {' '}
                        <Text variant="p2" isMedium>
                            {subtitle}
                        </Text>
                    </div>
                )}
            </div>

            <Link isBold textVariant="moduleCard" isArrowSmall link={'#'}>
                {linkText}
            </Link>
            <div className={styles.badgeWrap}>
                {arrayProfessions.map((badge, index) => (
                    <Fragment key={index}>
                        <Badge>
                            <Text variant="p2" isMedium>
                                {badge}
                            </Text>
                        </Badge>{' '}
                    </Fragment>
                ))}
            </div>
        </div>
    );
};

export default CardModule;
