import React from 'react';
import styles from './CardProfessions.module.css';
import Card from '../Card/Card';
import TextWithBackground from '../Text/textWithBackground/TextWithBackground';
import { useGetImage } from '../../utils/hooks/useGetImage';
import { showPicture } from '../../utils/showPicture';

type Card = {
    textTitle: string;
    textSubtitle: string;
    imgUrl: string;
    imgBackUrl: string;
    imgUrlHover: string;
    imgBackHover: string;
};

interface ICardProfessions {
    blockData: Card[];
}

const CardProfessions: React.FC<ICardProfessions> = ({ blockData }) => {
    const imgCardsUrlList = useGetImage('/img/cards');
    React.useEffect(() => {
        console.log("window.innerHeight", window.innerWidth);
      }, []);
    const cardList = blockData.map((item, index) => (
        <div className={styles.CardItem} key={index}>
            {/* <Card
                textTitle={item.textTitle}
                titleFont={'blockCard'}
                textSubtitle={item.textSubtitle}
                widthCard={160}
                heigthCard={165}
                cardStyle={'cardProf'}
                imgUrl={showPicture(item.imgUrl, imgCardsUrlList)}
                imgBackUrl={item.imgBackUrl}
                imgUrlHover={item.imgUrlHover}
                imgBackHover={item.imgBackHover}
            /> */}
        </div>
    ));

    return (
        <>
            <div className={styles.Container}>
                <div className={styles.CardProfessionsTextWrap}>
                    <TextWithBackground
                        isBold
                        variant="h2"
                        textsArray={[
                            { text: 'Профессии c ' },
                            { text: 'гарантией', withBackground: true },
                            { text: ' трудоустройства', withBackground: true },
                        ]}
                    />
                </div>
                <div className={styles.CardsWrap}>{cardList}</div>
            </div>
        </>
    );
};
export default CardProfessions;
