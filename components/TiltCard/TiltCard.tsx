import React from 'react';
//styles
import styles from './TiltCard.module.css';

interface ITiltCardProps {
    deg: number;
    imgUrl: string;
    top: number;
    left: number;
    link?: string;
}

const TiltCard: React.FC<ITiltCardProps> = ({ deg, imgUrl, top, left, link }) => (
    <div style={{ transform: `rotate(${deg}deg)` }} className={styles.tiltCard}>
        <a href={link} target={'_blank'} rel="noopener noreferrer">
            <img
                src={imgUrl}
                style={{
                    transform: `rotate(${-deg}deg)`,
                    position: 'absolute',
                    top: `${top}px`,
                    left: `${left}px`,
                    objectFit: 'contain',
                }}
                className={styles.img}
                alt=""
            />
        </a>
    </div>
);

export default TiltCard;
