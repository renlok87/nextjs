import React from 'react';

//styles
import styles from './TiltCards.module.css';
import classNames from 'classnames/bind';

//components
import TiltCard from '../TiltCard/TiltCard';
import TextWithBackground from '../Text/textWithBackground/TextWithBackground';
import Text from '../Text/Text';

const cx = classNames.bind(styles);

type TiltCardProps = {
    link1: string;
    link2: string;
    link3: string;
    link4: string;
};

interface ITiltCards {
    blockData: TiltCardProps;
}

const TiltCards: React.FC<ITiltCards> = ({ blockData }) =>{
    React.useEffect(() => {
        console.log("window.innerHeight", window.innerWidth);
      }, []); return (
    
    <div className={styles.tiltCardsContainer}>
        <div className={styles.tiltCardsHalf}>
            <div className={cx(styles.tiltCardsPart, styles.partOne)}>
                <div className={styles.TextContainer}>
                    <div className={styles.TextTitleWrapper}>
                        <Text variant="tiltCardHeader">Оплачиваемая практика</Text>
                    </div>
                    <div className={styles.TextDescWrapper}>
                        <TextWithBackground
                            variant="p2"
                            textsArray={[
                                { text: 'Лучшие студенты попадают на ' },
                                { text: 'оплачиваемую практику', withBackground: true },
                                { text: ' в студию школу и ' },
                                { text: 'отбивают деньги', withBackground: true },
                                { text: ', еще до завершения обучения' },
                            ]}
                            isBold
                        />
                    </div>
                </div>
                <TiltCard
                    link={blockData.link1}
                    deg={-2}
                    imgUrl={'/img/tiltCardImgs/1.webp'}
                    top={15}
                    left={0}
                />
            </div>
            <div className={cx(styles.tiltCardsPart, styles.partTwo)}>
                <div className={styles.cardWrapper}>
                    <TiltCard
                        link={blockData.link2}
                        deg={2}
                        imgUrl={'/img/tiltCardImgs/certificate.webp'}
                        top={10}
                        left={18}
                    />
                </div>

                <div className={styles.textWrapper}>
                    <Text variant="tiltCardHeader">Сертификаты после каждого модуля</Text>
                </div>
            </div>
        </div>
        <div className={styles.tiltCardsHalf}>
            <div className={cx(styles.tiltCardsPart, styles.partThree)}>
                <div>
                    <Text variant="tiltCardHeader">Резюме</Text>
                </div>
                <TiltCard
                    link={blockData.link3}
                    deg={-2}
                    imgUrl={'/img/tiltCardImgs/3.webp'}
                    top={-30}
                    left={-4}
                />
            </div>
            <div className={cx(styles.tiltCardsPart, styles.partFour)}>
                <div className={styles.cardWrapper}>
                    <TiltCard
                        link={blockData.link4}
                        deg={2}
                        imgUrl={'/img/tiltCardImgs/4.webp'}
                        top={-20}
                        left={30}
                    />
                </div>

                <div className={styles.textWrapper}>
                    <Text variant="tiltCardHeader">Работы в портфолио</Text>
                    <Text variant="p2" isBold>
                        Создай сайт, серверное приложение или игру
                    </Text>
                </div>
            </div>
        </div>
    </div>
)};

export default TiltCards;
