import React from 'react';
import styles from './BannerLesson.module.css';
import Button from '../Button/Button';
import { Func } from '@tinkoff/utils/typings/types';
import Text from '../Text/Text';

type BannerLessonProps = {
    btnText?: string;
    bannerLessonTitle?: string;
    bannerLessonSubtitle: string;
    textLink: string;
};

interface IBannerLessonProps {
    hasStar?: boolean;
    handleOpenLesson?: Func;
    blockData: BannerLessonProps;
}

const createMarkup = <T,>(text: T) => {
    return { __html: text };
};

const BannerLesson: React.FC<IBannerLessonProps> = ({ handleOpenLesson, blockData }) => {
    return (
        <div className={styles.wrap}>
            <div className={styles.BannerLesson}>
                <div className={styles.BannerLessonContent}>
                    <Text variant="bannerLessonTitle">{blockData.bannerLessonTitle}</Text>
                    <div className={styles.BannerLessonDescWrap}>
                        <Text variant="bannerLessonSubtitle">
                            <div
                                dangerouslySetInnerHTML={createMarkup(
                                    blockData.bannerLessonSubtitle,
                                )}
                            />
                        </Text>
                    </div>

                    <div className={styles.BannerLessonBtnWrap}>
                        <form action={blockData.textLink} target="_blank">
                            <Button
                                onClick={handleOpenLesson}
                                arrow
                                customClass="animateBtn"
                            >
                                <span>{blockData.btnText}</span>
                            </Button>
                        </form>
                    </div>
                </div>
                <div className={styles.star} />
                <div className={styles.starText} />
                <div className={styles.BannerLessonBackground} />
            </div>
        </div>
    );
};

export default BannerLesson;
