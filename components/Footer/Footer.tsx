import React from 'react';
//styles
import styles from './Footer.module.css';
import classNames from 'classnames/bind';
//components
import WebHeroLogo from '../WebHeroLogo/WebHeroLogo';
import Text from '../Text/Text';
import SignInButton from '../SignInButton/SignInButton';
import NetworkButtons from '../NetworkButtons/NetworkButtons';

const cx = classNames.bind(styles);

const Footer: React.FC = () => (
    <footer>
        <div className={styles.horizontalLine} />
        <div className={styles.footerContainer}>
            <div className={styles.footerElem}>
                <WebHeroLogo type="circlePurple" />
                <div className={styles.smallBtn}>
                    <SignInButton isYellowButton isBigButton isButtonTitle isFooterButton>
                        Личный кабинет
                    </SignInButton>
                </div>
            </div>
            <div className={cx(styles.footerElem, styles.footerInfoWrapper)}>
                <div className={cx(styles.footerInfo, styles.footerInfoLinks)}>
                    <div className={cx(styles.linkBlock, styles.fBlock)}>
                        <a href="#top" target="_blank" className={styles.footerInfoLink}>
                            <Text variant="p2">Курсы и модули</Text>
                        </a>
                        <a href="#top" target="_blank" className={styles.footerInfoLink}>
                            <Text variant="p2">Отзывы</Text>
                        </a>
                        <a href="#top" target="_blank" className={styles.footerInfoLink}>
                            <Text variant="p2">Для партнеров и работодателей</Text>
                        </a>
                        <a
                            href="#top"
                            target="_blank"
                            className={cx(styles.footerInfoLink, styles.additem)}
                        >
                            <Text variant="p2">Офферта</Text>
                        </a>
                    </div>
                    <div className={cx(styles.linkBlock, styles.sBlock)}>
                        <a href="#top" target="_blank" className={styles.footerInfoLink}>
                            <Text variant="p2">Бесплатный марафон</Text>
                        </a>
                        <a href="#top" target="_blank" className={styles.footerInfoLink}>
                            <Text variant="p2">Вакансии школы</Text>
                        </a>
                        <a href="#top" target="_blank" className={styles.footerInfoLink}>
                            <Text variant="p2">Написать нам</Text>
                        </a>
                        <a
                            href="#top"
                            target="_blank"
                            className={cx(styles.footerInfoLink, styles.additem)}
                        >
                            <Text variant="p2">Политика конфиденциальности</Text>
                        </a>
                    </div>
                    <div className={cx(styles.linkBlock, styles.tBlock)}>
                        <a href="#top" target="_blank" className={styles.footerInfoLink}>
                            <Text variant="p2">Офферта</Text>
                        </a>
                        <a href="#top" target="_blank" className={styles.footerInfoLink}>
                            <Text variant="p2">Политика конфиденциальности</Text>
                        </a>
                    </div>
                </div>
                <div className={cx(styles.footerInfo, styles.bigItem)}>
                    <Text variant="p2">Сайт разработан учениками школы (.❛ ᴗ ❛.)</Text>
                </div>
            </div>
            <div className={styles.rightBlock}>
                <div className={styles.bigBtn}>
                    <SignInButton isYellowButton isBigButton isButtonTitle>
                        Личный кабинет
                    </SignInButton>
                </div>
                <NetworkButtons
                    types={['FB', 'VK', 'IN', 'YT', 'TG']}
                    size={24}
                    customClass={styles.btnWrap}
                />

                <div className={cx(styles.footerInfo, styles.additem)}>
                    <Text variant="p2">Сайт разработан учениками школы (.❛ ᴗ ❛.)</Text>
                </div>
            </div>
        </div>
    </footer>
);

export default Footer;
