import React from 'react';
import styles from './CardFormat.module.css';
import Card from '../Card/Card';
import Text from '../Text/Text';

const TITLE = 'Гибкий формат обучения';
const items = [
    {
        textTitle: 'Онлайн',
        textSubtitle: 'Учись из любой точки мира, под одеялом или во время поездки',
        imgUrl: '/img/cards/online.svg',
        imgBackUrl: '',
        imgUrlHover: '',
        imgBackHover: '',
    },
    {
        textTitle: 'Гибкий график',
        textSubtitle:
            'Учись тогда, когда удобно тебе. Не завись от темпа других студентов',
        imgUrl: '/img/cards/clock.svg',
        imgBackUrl: '',
        imgUrlHover: '',
        imgBackHover: '',
    },
    {
        textTitle: 'Доступ навсегда',
        textSubtitle: 'Учеба не ограничена по времени. Пожизненный доступ к материалам',
        imgUrl: '/img/cards/access.svg',
        imgBackUrl: '',
        imgUrlHover: '',
        imgBackHover: '',
    },
];
const CardFormat: React.FC = () => {
    React.useEffect(() => {
        console.log("window.innerHeight", window.innerWidth);
      }, []);
    const cardList = items.map(item => (
        <div className={styles.CardItem}>
            <Card
                textTitle={item.textTitle}
                titleFont={'blockCard'}
                textSubtitle={item.textSubtitle}
                widthCard={160}
                heigthCard={217}
                cardStyle={'cardFormat'}
                imgUrl={item.imgUrl}
                imgBackUrl={item.imgBackUrl}
                imgUrlHover={item.imgUrlHover}
                imgBackHover={item.imgBackHover}
            />
        </div>
    ));

    return (
        <>
            <div className={styles.Container}>
                <div className={styles.CardFormatTextWrap}>
                    <Text variant="h2" isBold>
                        {TITLE}
                    </Text>
                </div>
                <div className={styles.CardsWrap}>{cardList}</div>
            </div>
        </>
    );
};
export default CardFormat;
