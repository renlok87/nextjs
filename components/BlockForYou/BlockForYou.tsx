import React, { useState } from 'react';
import styles from './BlockForYou.module.css';
import Text from '../Text/Text';
import Link from '../Link/Link';
import List from '../List/List';
import PopupQuestion from '../Popup/question/PopupQuestion';


interface IBlockForYouProps {
    link?: string;
}

const ITEM_FIRST = 'Ты студент';
const ITEM_SECOND = 'Тебя не устраивает текущий доход';
const ITEM_THRID = 'Ты ходишь на нелюбимую работу';
const ITEM_FOURTH = 'Работаешь в IT, но хотел бы зарабатывать больше';

const BlockForYou: React.FC<IBlockForYouProps> = ({ link }) => {
    const [showModal, setValue] = useState(false);
    React.useEffect(() => {
        console.log("window.innerHeight", window.innerHeight);
      }, []);
    const popupShow = () => {
        if (showModal)
            return (
                <PopupQuestion
                    isOpened={true}
                    onCloseHandler={() => setValue(false)}
                    title={'Задай нам вопрос'}
                    subtitle={'И мы отправим тебе программу обучения'}
                />
            );
    };

    return (
        <div className={styles.blockForYou}>
            {popupShow()}
            <Text variant="reviewHeader" isBold={true}>
                Это для тебя, если:
            </Text>
            <div className={styles.content}>
                <div className={styles.contentColumnFirst}>
                    <List arrayText={[ITEM_FIRST, ITEM_SECOND]} />
                </div>
                <div className={styles.contentColumnSecond}>
                    <List arrayText={[ITEM_THRID, ITEM_FOURTH]} />
                </div>
            </div>
            <Link
                textVariant={'link'}
                isBold={true}
                isArrowBig={true}
                onClick={() => setValue(true)}
            >
                Получить бесплатную консультацию
            </Link>
        </div>
    );
};

export default BlockForYou;
