import React, { useState } from 'react';
import Text from '../Text/Text';
import CardModule from '../CardModule/CardModule';
import styles from '../Modules/Modules.module.css';
import Link from '../Link/Link';
import Description from '../Popup/description/Description';

const TITLE = 'Модули';
const DESCRIPTION =
    'Модуль - часть профессии и развивает отдельный навык. Доступен отдельно';
const LINK = 'Все модули';

type CardModule = {
    title: string;
    subtitle: string;
    linkText: string;
    link: string;
    arrayProfessions: string[];
    id: number;
};

interface IModules {
    blockData: CardModule[];
}

const Modules: React.FC<IModules> = ({ blockData }) => {
    const [showModal, setValue] = useState(false);
    React.useEffect(() => {
        console.log("window.innerHeight", window.innerWidth);
      }, []);
    const cardList = blockData.map(item => (
        <CardModule
            id={item.id}
            key={item.id}
            onClick={() => setValue(true)}
            link={item.link}
            linkText={item.linkText}
            title={item.title}
            subtitle={item.subtitle}
            arrayProfessions={item.arrayProfessions}
        />
    ));

    const popupShow = () => {
        if (showModal)
            return (
                <Description
                    isOpened={true}
                    onCloseHandler={() => setValue(false)}
                    popupTitle={'Модуль'}
                    popupSubtitle={'Описание'}
                    popupLink={'#'}
                    popupLinkText={'Демо урок'}
                />
            );
    };

    return (
        <div className={styles.ModulesWrap}>
            <div>{popupShow()}</div>
            <section className={styles.modules}>
                <div className={styles.title}>
                    <Text variant="h2" isBold>
                        {TITLE}
                    </Text>
                </div>
                <div className={styles.description}>
                    <Text variant="p2" isMedium>
                        {DESCRIPTION}
                    </Text>
                </div>
                <div className={styles.cards}>{cardList}</div>
                <div className={styles.link}>
                    <Link
                        textVariant="link"
  
                        isArrowSmall
                        isBold
                        link={'#'}
                    >
                        {LINK}
                    </Link>
                </div>
            </section>
        </div>
    );
};

export default Modules;
