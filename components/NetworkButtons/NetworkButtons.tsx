import React from 'react';
//styles
import styles from './NetworkButtons.module.css';
import classNames from 'classnames/bind';
//icons
import FbIcon from '../../icons/FbIcon';
import VkIcon from '../../icons/VkIcon';
import YTIcon from '../../icons/YTIcon';
import TGIcon from '../../icons/TGIcon';
import INIcon from '../../icons/INIcon';

const cx = classNames.bind(styles);

type Network = 'VK' | 'FB' | 'TG' | 'IN' | 'YT';

interface INetworkButtonsProps {
    types: Array<Network>;
    urls?: { [key in Network]?: URL };
    size: 16 | 24 | 44;
    isFillGray?: boolean;
    marginBetweenBtn50?: boolean;
    customClass?: string;
}

interface IIconsList {
    [key: string]: (size: number) => JSX.Element;
}

const networks: IIconsList = {
    FB: (size: number) => <FbIcon size={size} />,
    VK: (size: number) => <VkIcon size={size} />,
    TG: (size: number) => <TGIcon size={size} />,
    YT: (size: number) => <YTIcon size={size} />,
    IN: (size: number) => <INIcon size={size} />,
};

const NetworkButtons: React.FC<INetworkButtonsProps> = ({
    types,
    urls,
    size,
    isFillGray,
    marginBetweenBtn50,
    customClass,
}) => {
    const buttons = types.map(item => {
        return (
            <a
                key={item}
                href={
                    urls && Object.keys(urls).length && urls[item]
                        ? urls[item]!.href
                        : '#top'
                }
                target="_blank"
                rel="noopener noreferrer"
                className={cx(styles.button, {
                    fillGray: isFillGray,
                    marginBetweenBtn50,
                })}
            >
                {networks[item](size)}
            </a>
        );
    });

    return <div className={cx(styles.buttons, `${customClass}`)}>{buttons}</div>;
};

export default NetworkButtons;
