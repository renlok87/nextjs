import React from 'react';
import styles from './CardComposition.module.css';
import Card from '../Card/Card';
import Text from '../Text/Text';
import Link from '../Link/Link';

const TITLE = 'Состав обучения';
const LINK = 'Узнать больше';
const items = [
    {
        textTitle: 'Онлайн- платформа',
        textSubtitle: 'Личный кабинет, уроки, задания и чаты под рукой',
        imgUrl: '/img/cards/notebook.svg',
        imgBackUrl: '',
        imgUrlHover: '',
        imgBackHover: '',
    },
    {
        textTitle: 'Понятные видео-уроки',
        textSubtitle: 'Не более 15 минут с доступным объяснением, microlearing',
        imgUrl: '/img/cards/tv.svg',
        imgBackUrl: '',
        imgUrlHover: '',
        imgBackHover: '',
    },
    {
        textTitle: 'Проверка работ',
        textSubtitle: 'Все работы проверяют практикующие разработчики',
        imgUrl: '/img/cards/CheckOk.svg',
        imgBackUrl: '',
        imgUrlHover: '',
        imgBackHover: '',
    },
    {
        textTitle: 'Code Review',
        textSubtitle:
            'Имитация реального рабочего процесса подготовит к работе в компании',
        imgUrl: '/img/cards/CardStar.svg',
        imgBackUrl: '',
        imgUrlHover: '',
        imgBackHover: '',
    },
    {
        textTitle: 'Тестирование',
        textSubtitle: 'Тест покажется на сколько хорошо был усвоен материал',
        imgUrl: '/img/cards/Dart.svg',
        imgBackUrl: '',
        imgUrlHover: '',
        imgBackHover: '',
    },
    {
        textTitle: 'Дополнительные материалы',
        textSubtitle: 'Для более глубокого погружения и создания шпаргалок на будущее',
        imgUrl: '/img/cards/books.svg',
        imgBackUrl: '',
        imgUrlHover: '',
        imgBackHover: '',
    },
    {
        textTitle: 'Чаты модулей',
        textSubtitle: 'Найдешь ответы на все свои вопросы и поддержку',
        imgUrl: '/img/cards/chat.svg',
        imgBackUrl: '',
        imgUrlHover: '',
        imgBackHover: '',
    },
    {
        textTitle: 'Мобильное приложение',
        textSubtitle: 'Смотри уроки и общайся с ментором даже в дороге',
        imgUrl: '/img/cards/smart.svg',
        imgBackUrl: '',
        imgUrlHover: '',
        imgBackHover: '',
    },
];

type CardCompositionDataProps = {
    link: string;
};

interface ICardComposition {
    blockData: CardCompositionDataProps;
}

const CardComposition: React.FC<ICardComposition> = ({ blockData }) => {
    React.useEffect(() => {
        console.log("window.innerHeight", window.innerWidth);
      }, []);
    const cardList = items.map(item => (
        <div className={styles.CardItem}>
            <Card
                textTitle={item.textTitle}
                titleFont={'h5'}
                textSubtitle={item.textSubtitle}
                widthCard={160}
                heigthCard={260}
                cardStyle={'cardComposition'}
                imgUrl={item.imgUrl}
                imgBackUrl={item.imgBackUrl}
                imgUrlHover={item.imgUrlHover}
                imgBackHover={item.imgBackHover}
            />
        </div>
    ));

    return (
        <>
            <div className={styles.Container}>
                <div className={styles.CardCompositionTextWrap}>
                    <Text variant="h3" isBold>
                        {TITLE}
                    </Text>
                </div>
                <div className={styles.CardsWrap}>{cardList}</div>
                <div className={styles.link}>
                    <Link
                        textVariant="link"
                        isArrowSmall
                        isBold
                        link={blockData.link}
                    >
                        {LINK}
                    </Link>
                </div>
            </div>
        </>
    );
};
export default CardComposition;
