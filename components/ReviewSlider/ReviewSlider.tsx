import React, { useState } from 'react';
//styles
import styles from './ReviewSlider.module.css';
import classNames from 'classnames/bind';
//components
import Text from '../Text/Text';
import NetworkButtons from '../NetworkButtons/NetworkButtons';

//icons
//data

import PopupVideo from '../Popup/Video/PopupVideo';
interface IDummy {
    title: string;
    name: string;
    city: string;
    network: 'FB' | 'VK';
    networksUrl: string;
    before: {
        first: string;
        second: string;
        third: string;
    };

    after: {
        first: string;
        second: string;
    };
    imgURL: string;
    reviewVideoURL: string;
}

const dummyData: IDummy[] = [
    {
        title: 'Сменить работу строителя на профессию веб-разработчика в Лондоне',
        name: 'Даниэль Бевзюч',
        city: 'Лондон',
        network: 'FB',
        networksUrl: 'https://facebook.com',
        before: {
            first: 'Работал на стройке.',
            second: 'Не устраивали условия работы',
            third: 'и риски здоровью',
        },
        after: {
            first: 'Устроился веб-разработчиком',
            second: 'в компанию Techtiq.co.uk',
        },
        imgURL: '/img/reviewPhotos/1.png',
        reviewVideoURL: 'https://www.youtube.com/embed/2IpToc8e0Ck',
    },
    {
        title: 'Сменить работу строителя на профессию веб-разработчика в Лондоне 2',
        name: 'Даниэль Бевзюч',
        city: 'Лондон',
        network: 'FB',
        networksUrl: 'https://facebook.com',
        before: {
            first: 'Работал на стройке.',
            second: 'Не устраивали условия работы',
            third: 'и риски здоровью',
        },
        after: {
            first: 'Устроился веб-разработчиком',
            second: 'в компанию Techtiq.co.uk',
        },
        imgURL: '/img/reviewPhotos/1.png',
        reviewVideoURL: 'https://www.youtube.com/embed/2IpToc8e0Ck',
    },
    {
        title: 'Сменить работу строителя на профессию веб-разработчика в Лондоне 3',
        name: 'Даниэль Бевзюч',
        city: 'Лондон',
        network: 'FB',
        networksUrl: 'https://facebook.com',
        before: {
            first: 'Работал на стройке.',
            second: 'Не устраивали условия работы',
            third: 'и риски здоровью',
        },
        after: {
            first: 'Устроился веб-разработчиком',
            second: 'в компанию Techtiq.co.uk',
        },
        imgURL: '/img/reviewPhotos/1.png',
        reviewVideoURL: 'https://www.youtube.com/embed/2IpToc8e0Ck',
    },
];

const cx = classNames.bind(styles);

const ReviewSlider: React.FC = () => {
    const [checkNumber, setCheckNumber] = useState(0);
    const [isVideoOpened, setIsVideoOpened] = useState(false);
    React.useEffect(() => {
        console.log("window.innerHeight", window.innerWidth);
      }, []);
    const toggleVideoHandler = () => setIsVideoOpened(prev => !prev);

    const slides: JSX.Element[] = dummyData.map((data, index) => (
        <div key={index} className={styles.ReviewContainer}>
            <input
                id={`sliderBtn${index}`}
                className={styles.radioBtn}
                type="radio"
                name="slider"
                checked={index === checkNumber}
                onChange={_e => setCheckNumber(index)}
            />
            <div className={styles.reviewSlide}>
                <div className={styles.titleWrapper}>
                    <Text variant="reviewTitle">{data.title}</Text>
                </div>
                <div className={styles.resultInfoGridContainer}>
                    <div className={styles.resultFlexContainer}>
                        <div className={styles.beforeAfter}>
                            <Text variant="sliderDecorText">До</Text>
                        </div>
                        <div className={styles.before}>
                            <Text variant="p2" isBold>
                                {data.before.first}
                                <br />
                                {data.before.second}
                                <br />
                                {data.before.third}
                                <br />
                            </Text>
                        </div>
              
                        <div className={styles.beforeAfter}>
                            <Text variant="sliderDecorText">После</Text>
                        </div>
                        <div>
                            <Text variant="p2" isBold>
                                {data.after.first}
                                <br />
                                {data.after.second}
                            </Text>
                        </div>
                    </div>
                </div>
                <div className={styles.reviewer}>
                    <div className={styles.reviewerPhotoWrapper}>
                        <img
                            className={styles.reviewerPhoto}
                            src={data.imgURL}
                            alt={`Видеоотзыв от ${data.name}`}
                            title={`Видеоотзыв от ${data.name}`}
                            onClick={toggleVideoHandler}
                        />
              
                    </div>
                    <div className={styles.reviewerInfo}>
                        <div className={styles.reviewerContacts}>
                            <NetworkButtons
                                types={[data.network]}
                                size={16}
                                isFillGray
                                urls={{ [data.network]: data.networksUrl }}
                            />
                            <Text variant="p2" isBold>
                                {data.name}
                            </Text>
                        </div>
                        <Text variant="p2">{data.city}</Text>
                    </div>
                </div>
                
            </div>
            <PopupVideo  isOpened={index === checkNumber && isVideoOpened}
                onCloseHandler={toggleVideoHandler}
                urlVideo={data.reviewVideoURL}/>
        </div>
    ));

    const labels = dummyData.map((_, index) => (
        <label
            key={index}
            className={cx(styles.radioLabel, {
                checkedLabel: index === checkNumber,
            })}
            htmlFor={`sliderBtn${index}`}
        />
    ));

    return (
        <div className={styles.wrap}>
            <div className={styles.reviewHeader}>
                <Text variant="reviewHeader">Наша гордость</Text>
            </div>
            {slides}
            <div className={styles.radioLabels}>{labels}</div>
        </div>
    );
};

export default ReviewSlider;
