import React from 'react';
import { Func } from '@tinkoff/utils/typings/types';
import styles from './Button.module.scss';
import ArrowIcon from '../../icons/ArrowIcon';
import className from 'classnames/bind';

const cx = className.bind(styles);

interface IButtonProps {
    onClick?: Func;
    arrow: boolean;
    customClass?: string;
}

const Button: React.FC<IButtonProps> = ({ arrow, onClick, children, customClass }) => (
    <button onClick={onClick} className={cx(styles.button, [`${customClass}`])}>
        {children}
        {arrow && (
            <span className={styles.arrow}>
                <ArrowIcon />
            </span>
        )}
    </button>
);

export default Button;
