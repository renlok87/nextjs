import React from 'react';
import styles from './HeaderGif.module.css';
import Star from '../Star/Star';

interface IHeaderGifTimeProps {
    imgBackUrl: string;
}

const HeaderGifTime: React.FC<IHeaderGifTimeProps> = ({ imgBackUrl }) => {
    return (
        <div className={styles.gifWrap}>
            <div className={styles.gif}>
                <Star
                    starsStep={'step1'}
                    imgUrl={'img/gif/giphy-9.gif'}
                    isHeaderStar={'true'}
                />
            </div>
            <div
                className={styles.backTime}
                style={{
                    backgroundImage: `url(${imgBackUrl})`,
                }}
            />
        </div>
    );
};

export default HeaderGifTime;
