import React from 'react';
import styles from './HeaderGif.module.css';
import Star from '../Star/Star';

interface IHeaderGifWorkProps {
    imgBackUrl: string;
}

const HeaderGifWork: React.FC<IHeaderGifWorkProps> = ({ imgBackUrl }) => {
    return (
        <div className={styles.gifWrap}>
            <div className={styles.gif}>
                <Star
                    starsStep={'step1'}
                    imgUrl={'img/gif/200-1.gif'}
                    isHeaderStar={'true'}
                />
            </div>
            <div
                className={styles.backWork}
                style={{
                    backgroundImage: `url(${imgBackUrl})`,
                }}
            />
        </div>
    );
};

export default HeaderGifWork;
