import React, { useState } from 'react';
import styles from './Card.module.css';
import Text, { ITextProps } from '../Text/Text';
import className from 'classnames/bind';
import Star from '../Star/Star';
import { createMarkup } from '../../utils/createMarkup';

const cx = className.bind(styles);

interface ICardProps {
    textTitle: string;
    textSubtitle: string;
    cardStyle: string;
    widthCard: number;
    heigthCard: number;
    imgUrl: string;
    imgBackUrl: string;
    imgUrlHover: string;
    imgBackHover?: string;
    titleFont?: ITextProps['variant'];
}

const defaultProps = {
    widthCard: 261,
    heigthCard: 352,
};

const Card: React.FC<ICardProps> = ({
    textTitle,
    textSubtitle,
    cardStyle,
    widthCard,
    heigthCard,
    imgUrl,
    imgBackUrl,
    imgUrlHover,
    titleFont,
}) => {
    const [hovered, setHovered] = useState(false);

    const handleHover = () => {
        if (imgUrlHover === '') {
            return setHovered(false);
        }

        return setHovered(!hovered);
    };

    return (
        <div
            className={cx('card', cardStyle)}
            style={{
                maxWidth: `${widthCard}px`,
                minHeight: `${heigthCard}px`,
                boxShadow: hovered ? '0 15px 20px rgba(0,0,0,0.1)' : 'none',
            }}
            onMouseLeave={handleHover}
            onMouseEnter={handleHover}
        >
            <div className={styles.cardContent}>
                {!hovered ? (
                    <div
                        className={styles.cardImg}
                        style={{
                            backgroundImage: `url(${imgUrl}), url(${imgBackUrl})`,
                            fill: 'black',
                            zIndex: 5,
                        }}
                    ></div>
                ) : (
                    <div className={styles.cardImg} style={{ fill: 'black' }}>
                        <Star imgUrl={imgUrlHover} starsStep="step3" />
                    </div>
                )}
                <div className={styles.cardTitle}>
                    <Text variant={titleFont || 'h4'}>
                        <div dangerouslySetInnerHTML={createMarkup(textTitle)} />
                    </Text>
                </div>
                <Text variant={'p2'}>
                    <div dangerouslySetInnerHTML={createMarkup(textSubtitle)} />{' '}
                </Text>
            </div>
        </div>
    );
};

Card.defaultProps = defaultProps;

export default Card;
