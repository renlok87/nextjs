import React from 'react';
import styles from './Teacher.module.css';

interface ITeacherProps {
    urlFotoTeacher: string;
    urlBackgroundTeacher?: string;
}

const Teacher: React.FC<ITeacherProps> = ({ urlFotoTeacher, urlBackgroundTeacher }) => {
    return (
        <div className={styles.teacherWrap}>
            <div
                style={{
                    background: `url(${urlFotoTeacher}) bottom right no-repeat , url(${urlBackgroundTeacher}) -1px 2px no-repeat`,
                    backgroundSize: '95%',
                }}
                className={styles.teacher}
            ></div>
        </div>
    );
};

export default Teacher;
