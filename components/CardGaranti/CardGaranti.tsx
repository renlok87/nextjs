import React from 'react';
import styles from './CardGaranti.module.css';
import Text from '../Text/Text';
import Card from '../Card/Card';
import TextWithBackground from '../Text/textWithBackground/TextWithBackground';
import { useGetImage } from '../../utils/hooks/useGetImage';
import { showPicture } from '../../utils/showPicture';

const TEXT_CARD_HEADER = 'Гарантии';

type CardGarantiProps = {
    imgBackHover: string;
    imgBackUrl: string;
    imgUrlHover: string;
    imgUrl: string;
    textSubtitle: string;
    textTitle: string;
};

interface ICardGaranti {
    blockData: CardGarantiProps[];
    cardStyle?: string;
}

const CardGaranti: React.FC<ICardGaranti> = ({ blockData }) => {
    const imgCardsUrlList = useGetImage('/img/cards');
    React.useEffect(() => {
        console.log("window.innerHeight", window.innerWidth);
      }, []);
    const cardList = blockData.map((item, index) => (
        <div className={styles.CardItem} key={index}>
            <Card
                textTitle={item.textTitle}
                titleFont={'blockCard'}
                textSubtitle={item.textSubtitle}
                widthCard={160}
                heigthCard={295}
                cardStyle={'cardGarant'}
                imgUrl={showPicture(item.imgUrl, imgCardsUrlList)}
                imgBackUrl={showPicture(item.imgBackUrl, imgCardsUrlList)}
                imgUrlHover={showPicture(item.imgUrlHover, imgCardsUrlList)}
                imgBackHover={showPicture(item.imgBackHover, imgCardsUrlList)}
            />
        </div>
    ));

    return (
        <>
            <div className={styles.Container}>
                <div className={styles.TextWrapHeader}>
                    <Text variant="h2">{TEXT_CARD_HEADER}</Text>
                </div>
                <div className={styles.TextWrapParagraph}>
                    <TextWithBackground
                        isBold={false}
                        variant="textCardParagraph"
                        textsArray={[
                            { text: 'Перед оплатой  ' },
                            { text: 'заключим договор ', withBackground: true },
                            { text: ' публичной оферты с указанием гарантий' },
                        ]}
                    />
                </div>
                <div className={styles.CardsWrap}>{cardList}</div>
            </div>
        </>
    );
};
export default CardGaranti;
