import React from 'react';
import styles from './Badge.module.css';

const Badge: React.FC = ({ children }) => (
    <span className={styles.badge}>{children}</span>
);

export default Badge;
