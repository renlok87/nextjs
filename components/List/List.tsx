import React from 'react';
import styles from './List.module.css';
import Text from '../Text/Text';

interface IListProps {
    arrayText: string[];
}

const List: React.FC<IListProps> = ({ arrayText }) => {
    return (
        <ul className={styles.contentList}>
            {arrayText.map((text, index) => (
                <li className={styles.contentItem} key={index}>
                    <div className={styles.contentTextLine}></div>
                    <div className={styles.contentText}>
                        <Text variant="p2" isBold={false}>
                            {text}
                        </Text>
                    </div>
                </li>
            ))}
        </ul>
    );
};

export default List;
