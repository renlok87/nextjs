import React from 'react';
import styles from './PopupQuestion.module.css';
import Popup from '../Popup';
import { Func } from '@tinkoff/utils/typings/types';
import NetworkButtons from '../../NetworkButtons/NetworkButtons';

interface IPopupQuestionProps {
    isOpened: boolean;
    onCloseHandler: Func;
    title: string;
    subtitle: string;
}

const PopupQuestion: React.FC<IPopupQuestionProps> = ({
    isOpened,
    onCloseHandler,
    title,
    subtitle,
}) => {
    return (
        <Popup
            isOpened={isOpened}
            onCloseHandler={onCloseHandler}
            popupTitle={title}
            popupSubtitle={subtitle}
        >
            <div className={styles.btnWrap}>
                <NetworkButtons
                    types={['FB', 'VK']}
                    size={44}
                    marginBetweenBtn50={true}
                />
            </div>
        </Popup>
    );
};

export default PopupQuestion;
