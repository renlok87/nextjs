import React from 'react';
import classnames from 'classnames/bind';
import { Func } from '@tinkoff/utils/typings/types';
import AwesomeSlider from 'react-awesome-slider';
import styles from './PopupSlider.module.css';

const cx = classnames.bind(styles);

interface IPopupSlider {
    isOpened: boolean;
    onCloseHandler: Func;
    sliderPictures?: Array<string>;
}

const PopupSlider: React.FC<IPopupSlider> = ({
    onCloseHandler,
    isOpened,
    sliderPictures,
}) => {
    return (
        <div className={cx('popupWrap', { popupClose: !isOpened })}>
            <div className={styles.popupOverlay} onClick={onCloseHandler}></div>
            <div className={styles.sliderWrap}>
                <button onClick={onCloseHandler} className={styles.btnClose} />
                <AwesomeSlider
                    bullets={false}
                    animation="openAnimation"
                    organicArrows={false}
                >
                    {sliderPictures
                        ? sliderPictures.map(item => <div data-src={item} key={item} />)
                        : 'Not Found Pictures'}
                </AwesomeSlider>
            </div>
        </div>
    );
};

export default PopupSlider;
