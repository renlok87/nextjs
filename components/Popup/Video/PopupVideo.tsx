import React from 'react';
import styles from './PopupVideo.module.css';
import Popup from '../Popup';
import { Func } from '@tinkoff/utils/typings/types';
import Text from '../../Text/Text';

interface IPopupVideoProps {
    isOpened: boolean;
    onCloseHandler: Func;
    urlVideo?: string;
}
const TEXT_NOT_VIDEO_FOUND = 'Видео потерялось( Но мы его обязательно найдем';

const PopupVideo: React.FC<IPopupVideoProps> = ({
    isOpened,
    onCloseHandler,
    urlVideo,
}) => {
    return (
        <Popup isOpened={isOpened} onCloseHandler={onCloseHandler}>
            {urlVideo ? (
                <div className={styles.videoWrap}>
                    <iframe
                        title="PopupVideo"
                        width="100%"
                        height="100%"
                        src={urlVideo}
                        frameBorder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowFullScreen
                    ></iframe>
                </div>
            ) : (
                <div>
                    <Text variant="h4" isBold={true}>
                        {TEXT_NOT_VIDEO_FOUND}
                    </Text>
                </div>
            )}
        </Popup>
    );
};

export default PopupVideo;