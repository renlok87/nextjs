import React from 'react';
import { Func } from '@tinkoff/utils/typings/types';
import Popup from '../Popup';

interface IDescriptionProps {
    isOpened: boolean;
    onCloseHandler: Func;
    popupTitle: string;
    popupSubtitle: string;
    popupLink: string;
    popupLinkText: string;
}
const Description: React.FC<IDescriptionProps> = ({
    isOpened,
    onCloseHandler,
    popupTitle,
    popupSubtitle,
    popupLink,
    popupLinkText,
}) => {
    return (
        <Popup
            popupTitle={popupTitle}
            popupSubtitle={popupSubtitle}
            popupLink={popupLink}
            popupLinkText={popupLinkText}
            isOpened={isOpened}
            onCloseHandler={onCloseHandler}
        />
    );
};

export default Description;
