import React from 'react';
import styles from './PopupTeacher.module.css';
import Popup from '../Popup';
import Teacher from '../../FotoTeacher/Teacher';
import Text from '../../Text/Text';
import { Func } from '@tinkoff/utils/typings/types';

interface IPopupTeacherProps {
    isOpened: boolean;
    onCloseHandler: Func;
    popupTeacherTitle: string;
    popupTeacherSubtitle: string;
    urlFotoTeacher: string;
    urlBackgroundTeacher?: string;
    popupTeacherAbout?: string;
    logos_img?: string[];
    title?: string;
}

const PopupTeacher: React.FC<IPopupTeacherProps> = ({
    isOpened,
    onCloseHandler,
    popupTeacherTitle,
    popupTeacherSubtitle,
    popupTeacherAbout,
    urlFotoTeacher,
    logos_img,
    urlBackgroundTeacher,
    title,
}) => {
    return (
        <Popup isOpened={isOpened} onCloseHandler={onCloseHandler}>
            <div className={styles.teacher}>
                <div className={styles.teacherLogo}>
                    <Teacher
                        urlFotoTeacher={urlFotoTeacher}
                        urlBackgroundTeacher={urlBackgroundTeacher}
                    />
                </div>
                <div className={styles.teacherContent}>
                    <Text variant="h4" isBold={true}>
                        {popupTeacherTitle}
                    </Text>
                    <Text variant="p2" isBold={true}>
                        {popupTeacherSubtitle}
                    </Text>
                    <Text variant="p2" isBold={true}>
                        {popupTeacherAbout}
                    </Text>
                    {logos_img?.length !== 0 ? (
                        <div className={styles.teacherLogoFirms}>
                            {logos_img?.map((img, index) => (
                                <div
                                    key={index}
                                    style={{ backgroundImage: `url(${img})` }}
                                    className={styles.teacherLogoFirmsFourth}
                                ></div>
                            ))}
                        </div>
                    ) : null}
                </div>
            </div>
        </Popup>
    );
};

export default PopupTeacher;
